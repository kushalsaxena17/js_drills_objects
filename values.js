module.exports=function values(obj){

    if (typeof obj !== "object" ) {
      return null;
    }
    const values = [];
    for ( let key in obj ) {
      if(typeof obj[key] === "function"){
          return null;
      }
      else{
          values.push(obj[key]);
      }
    }
    return values;
  };
  