module.exports=function invert(obj){
    if (typeof obj !== "object") {
      return null;
    }
    let obj2 = {};
    for (let val in obj) {
        obj2[obj[val]]=val;
    }
    return obj2;
  };
  