module.exports=function mapObject(obj, cb){

    if (typeof obj !== "object") {
      return null;
    }
    let arr = [];
    for (let key in obj){
      let temp = obj[key];
      if (typeof obj[key] === "number") 
        obj[key] = cb(obj[key]);
    }
    return obj;
  
  };
  