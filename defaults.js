module.exports=function defaults(obj,obj2){
    if (typeof obj !== "object" || obj === null || typeof obj2 !== "object" || obj2 === null) {
        return null;
      }
      for (let key in obj) {
          if(obj[key] === undefined){
              if(obj2[key]){
                  obj[key]=obj2[key];
              }
          }
      }
      return obj;
  


};