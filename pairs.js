module.exports=function pairs(obj){
    if (typeof obj !== "object" || obj === null) {
        return null;
      }
      let arr = [];
      for (let val in obj) {
        arr.push([val, obj[val]]);
      }
      return arr;
    
};
